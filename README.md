# VhackOs
VhackOs library I made for final project for cyb 205. 
<p>Written in Ruby 💗 </p>
<p> Easily to mod for other uses or use as is</p>



# FEATURES
* Spam Info
* Exploit Target
* Brute Force Target
* View Remote Log
* User Banking
* Buy Apps
* View Current Tasks
* Search For Ip
* Create User
* Network Scan
* Connection Manger
* Steal Money
* Miner
* Auto Steal
* Auto Upgrade
* Retry Tasks
* Auto Scan
* Packages
* Server Info
* Collect Packages
* Custom Log
* SDK
* Dowload Apps
* Blitzkreig

# Install gems
```
gem install colorize
gem install tty-table
gem install terminal-table
gem inatall pastel

```
# HOW TO RUN
```
ruby main_1.rb
```

# CONACT
```
  If you need to contact me use that email or you can find me around campus.
```


# IMAGES
<img src="https://i.imgur.com/nvoYNlU.png?1" alt="alt" align="center" height="400" width="200" title="VhackOS cheat"/>

# Contributing
You can contribute and improve the bot through the [Pull requests](https://github.com/crazy-ace003/VhackOs/compare) or open a [issue](https://github.com/crazy-ace003/VhackOs/issues/new).
